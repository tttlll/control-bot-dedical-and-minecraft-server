import json
from os.path import dirname as up

test=open(up((__file__))+'/config.yml','r')
admin=open(up((__file__))+'/admins.yml','r')
config=test.read().split('\n')
admin_list=admin.read()
admin.close()
test.close()
def token():
    for param in config:
        if param.split("=")[0] == 'token':
            token=param.split('=')[1]
            print('Token found!')
            return token
def group_id():
    for param in config:
        if param.split("=")[0] == 'group_id':
            group_id = int(param.split('=')[1])
            print('Group_id found!')
            return group_id
def v():
    for param in config:
        if param.split("=")[0] == 'v':
            print('Version found!')
            v = float(param.split('=')[1])

            return v


def admins():
    return json.loads(admin_list)

token=token()
group_id=group_id()
v=v()
admins=admins()