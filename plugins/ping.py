from plugins.ssh_server import commandos

def ping(user_id):
    main = ''
    for i in range(3):
        mas = ['ya.ru', 'ovh.com', 'google.com']
        message=(commandos('ping {} -c 4 -i 0.1 -q'.format(mas[i]),user_id))

        message = message.replace('--- ya.ru ping statistics ---', 'Страна: Россия 🇷🇺')
        message = message.replace('--- ovh.com ping statistics ---', 'Страна: Франция 🇲🇫')
        message = message.replace('--- google.com ping statistics ---', 'Страна : США 🇺🇸')

        message=message.replace('received','получено')
        message=message.replace('packets transmitted','отправлено')
        message=message.replace('rtt min/avg/max/mdev','Мин/сред/макс/отклон')
        message=message.replace('packet loss','потерянных')
        main=main+message

    return main
