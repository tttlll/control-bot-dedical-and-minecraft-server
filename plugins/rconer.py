from mcrcon import MCRcon
from plugins.reftext import texti
import set_config

def mainrcon(user_id,body):
    try:
        port=int(set_config.admins[str(user_id)]['server']['minecraft']['port'])
        host=set_config.admins[str(user_id)]['server']['minecraft']['host']
        password=set_config.admins[str(user_id)]['server']['minecraft']['password']

        with MCRcon(host, password,port) as mcr:
            resp = mcr.command(body)

            resp=texti(resp)

            if resp == '':
                resp = 'Команда выполнена, ответа не последовало.'

            message = 'Ответ сервера:\n'+resp
    except:
       message= 'Ошибка сервера\nВозможно сервер упал или нет доступа к нему.'
    return message

