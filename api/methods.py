import requests
import set_config
token=set_config.token
group_id=set_config.group_id
v=set_config.v

def send_message(peer_id,message,v,random_id,chat_id=None,keyboard=None):


    params={
        'user_id':peer_id,
        'message':message,
        'access_token':token,
        'v':v,
        'random_id':random_id,
        'keyboard':keyboard

    }
    url='https://api.vk.com/method/messages.send?'
    print(requests.get(url=url,params=params).text)


    url='https://api.vk.com/method/messages.send?user_id={}&message={}&access_token={}&v={}&random_id={}'\
        .format(peer_id,message,token,v,random_id)
    print( requests.get(url).text)

def getLongPollServer():
    url = 'https://api.vk.com/method/groups.getLongPollServer?group_id={}&access_token={}&v={}' \
        .format(group_id,token,v)
    data=requests.get(url).json()
    return data

