import time,requests
from api.methods import getLongPollServer
from api.methods import send_message
import set_config
import plugins.button as plugin
import plugins.ssh_server as ssh_server
import plugins.ping as ping
import plugins.rconer as rconer


group_id=set_config.group_id
v=set_config.v
data_vk=getLongPollServer()


if data_vk.get('response') != None:
    params = {
        'act': 'a_check',
        'key': data_vk.get('response')['key'],
        'ts': data_vk.get('response')['ts'],
        'wait': '25'
    }
    print('Успешный старт')
    while True:

        url = data_vk.get('response')['server']
        update = requests.get(url=url,params=params).json()
        print(update)
        if update.get("failed") != None:
            code_error=update.get('failed')
            data_vk = getLongPollServer()
            if code_error == 1:
                params.update({'ts':data_vk.get('response')['ts']})

            elif code_error == 2:
                params.update({'key': data_vk.get('response')['key']})

            elif code_error == 3:
                params.update({'key': data_vk.get('response')['key']})
                params.update({'ts': data_vk.get('response')['ts']})

            else:
                print(code_error)

        print(update)
        if (update.get('updates')) and update['updates'][0]['type']=='message_new':
            peer_id=update['updates'][0]['object']['peer_id']
            text=update['updates'][0]['object']['text']
            button = update.get('updates')[0]['object']
            button = button.get('payload')
            print(button)
            if text.lower()=='start':
                send_message(peer_id=peer_id,message='Выберете панель управления',random_id=time.time()*100,v=v,keyboard=plugin.control)
            if button!=None:
                button=eval(button)
                button=button.get('buttons')

                if set_config.admins.get(str(peer_id))==None:
                    send_message(peer_id=peer_id, message='Вы не админ :(', random_id=time.time() * 100,
                                 v=v)
                    params.update({'ts': update.get('ts')})
                    continue

                if button == 'dedical':
                    send_message(peer_id=peer_id, message='Выберете действие', random_id=time.time() * 100,
                                 v=v, keyboard=plugin.dedical)

                if button == 'back':
                    send_message(peer_id=peer_id, message='Выберете панель управления', random_id=time.time() * 100,
                                 v=v, keyboard=plugin.control)

                if button == 'reboot':
                    send_message(peer_id=peer_id, message='Перезагрузка...', random_id=time.time() * 100,
                                 v=v)
                    ssh_server.commandos('reboot',user_id=peer_id)

                if button == 'statistic':
                    CPU = ssh_server.commandos("""ps aux | awk '{s += $3} END {print s "%"}'""",user_id=peer_id)
                    MEM = ssh_server.commandos("""ps aux | awk '{s += $4} END {print s "%"}'""",user_id=peer_id)
                    send_message(peer_id=peer_id, message='🌐› Ваш процессор загружен на {}\n🌐› Ваша оперативная память загружена на {}'.format(CPU,MEM), random_id=time.time() * 100,
                                 v=v)

                if button == 'server':
                    send_message(peer_id=peer_id, message='Выберете действие', random_id=time.time() * 100,
                                 v=v, keyboard=plugin.server)

                if button == 'ping':
                    send_message(peer_id=peer_id, message=ping.ping(user_id=peer_id), random_id=time.time() * 100,
                                 v=v)

                if button == 'tps':
                    send_message(peer_id=peer_id, message=rconer.mainrcon(user_id=peer_id,body='tps'), random_id=time.time() * 100,
                                 v=v)
                if button == 'stop':
                    send_message(peer_id=peer_id, message=rconer.mainrcon(user_id=peer_id, body='list'),
                                 random_id=time.time() * 100,
                                 v=v)
                if button == 'server_reboot':
                    send_message(peer_id=peer_id, message=rconer.mainrcon(user_id=peer_id, body='reload confirm'),
                                 random_id=time.time() * 100,
                                 v=v)

        params.update({'ts': update.get('ts')})
